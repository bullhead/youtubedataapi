package com.haidar.youtubedata.domain.base;

import java.io.Serializable;

public class BaseSnippet implements Serializable {
    private String publishedAt;
    private String channelId;
    private String title;
    private String description;
    private ThumnailsWrapper thumbnails;
    private String channelTitle;

    public String getPublishedAt() {
        return publishedAt;
    }

    public void setPublishedAt(String publishedAt) {
        this.publishedAt = publishedAt;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ThumnailsWrapper getThumbnails() {
        return thumbnails;
    }

    public void setThumbnails(ThumnailsWrapper thumbnails) {
        this.thumbnails = thumbnails;
    }

    public String getChannelTitle() {
        return channelTitle;
    }

    public void setChannelTitle(String channelTitle) {
        this.channelTitle = channelTitle;
    }
}
