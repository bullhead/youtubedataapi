package com.haidar.youtubedata.domain.video;

import com.haidar.youtubedata.domain.base.BaseSnippet;

import java.io.Serializable;

public class VideoSnippet extends BaseSnippet implements Serializable {

    private String playlistId;
    private int position;
    private SnippetResource resourceId;


    public String getPlaylistId() {
        return playlistId;
    }

    public void setPlaylistId(String playlistId) {
        this.playlistId = playlistId;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public SnippetResource getResourceId() {
        return resourceId;
    }

    public void setResourceId(SnippetResource resourceId) {
        this.resourceId = resourceId;
    }
}
