package com.haidar.youtubedata.backend;

import android.support.annotation.NonNull;

import retrofit2.Call;
import retrofit2.Response;

public class Callback<T> implements retrofit2.Callback<T> {
    private OnApiResponse<T> completion;
    Callback(OnApiResponse<T> completion){
        this.completion=completion;
    }
    @Override
    public void onResponse(@NonNull Call<T> call,@NonNull Response<T> response) {
        if (completion!=null){
            completion.onResponse();
            T body=response.body();
            if (body!=null){
                completion.onSuccess(body);
            }else{
                completion.onError();
            }
        }
    }

    @Override
    public void onFailure(@NonNull Call<T> call,@NonNull Throwable t) {
        if (completion!=null){
            completion.onResponse();
            completion.onError();
        }
    }
}
